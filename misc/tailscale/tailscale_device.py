#!/usr/bin/env python3

import requests,os,json,pytz,sys
from datetime import datetime,timedelta
import logging

# Configure basic logging setup

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class TailscaleDeviceManager:
  """A basic way to view and delete Tailscale nodes using the REST API"""

  def __init__(self,api_key,tailnet):
    """Example:   
        tdm = TailscaleDeviceManager(api_key=os.environ['TAILSCALE_KEY'],tailnet=os.environ['TAILSCALE_TAILNET'])
    """

    self.api_key = api_key
    self.tailnet = tailnet
    self.headers = {'Authorization': f'Bearer {api_key}'}
    self.base_url = "https://api.tailscale.com/api/v2"
    self.device_dict = {}
    self.device_ids = []

  def update(self):
    """Connect to TailScale API and retrieve current list of devices, and populate device_dict and device_ids"""
    url = f'{self.base_url}/tailnet/{self.tailnet}/devices?fields=all'
    r = requests.get(url,headers=self.headers)

    if r.status_code == 200:
      raw_json = r.json()['devices']

      for d in raw_json:
        self.device_dict[d['id']] = d
        self.device_ids.append(d['id'])

      logger.info("Devices found:",len(raw_json))
      return True
    else:
      logger.error("No devices found or another problem")
      return False

  def dump(self):
    """Print out device name, last seen, and OS"""
    for i in self.device_dict.keys():
      d = self.device_dict[i]
      print (d['name'],d['lastSeen'],d['os'],d['clientConnectivity']['endpoints'])

  def get_device_ids(self):
    """Return a list of all the device IDs"""
    l = []
    for i in self.device_dict.keys():
      l.append(i)
    return l

  def delete_device(self,device_id):
    """Remove device from Tailscale"""
    url = f'https://api.tailscale.com/api/v2/device/{device_id}'
    return requests.delete(url,headers=self.headers)

  def delete_old(self,age=60):
    """Remove all devices older than a specified data"""
    timezone = pytz.timezone('UTC')
    now = datetime.now(pytz.timezone('UTC'))

    for d in self.device_ids:
      last_seen = self.device_dict[d]['lastSeen']
      logger.info(f"Device id: {d}{self.device_dict[d]['name']} last seen {self.device_dict[d]['lastSeen']}")
      last_date = datetime.strptime(last_seen, "%Y-%m-%dT%H:%M:%SZ")

      if (now - timezone.localize(last_date)) > timedelta(days=age):
        logger.info(f"Deleting: {self.device_dict[d]['name']} which was last seen on {last_seen}")
        self.delete_device(d)
      else:
        logger.info(f"Skipping {self.device_dict[d]['name']}")

# 
# TAILSCALE_KEY
# TAILSCALE_TAILNET
#

if __name__ == "__main__":
  tdm = TailscaleDeviceManager(api_key=os.environ['TAILSCALE_KEY'],tailnet=os.environ['TAILSCALE_TAILNET'])

  if tdm.update():
    tdm.dump()
    if "purge" in sys.argv:
      tdm.delete_old(45)


