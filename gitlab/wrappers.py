
import gitlab,string

def flat_issue(i):
  """ Return key fields from the issue"""

  attr_list = ['title','project_id','state','milestone','assignee','created_at','updated_at','closed_at','labels']

  i_list = []
  i_json = i.attributes

  # Normalize the data so we can put in an row/column structure

  for i in attr_list:
    if i == 'assignee':
      if i_json[i]:
        if 'username' in i_json[i]:
          i_list.append(i_json[i]['username'] )
    elif i == 'labels':
      pass
      #i_list.append('|'.join(i_json[i])) 
    else:
      i_list.append(i_json[i])

  return i_list
