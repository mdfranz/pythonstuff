#!/usr/bin/env python3

import gitlab,os,wrappers

gl = gitlab.Gitlab('https://gitlab.com',private_token=os.environ['GITLAB_TOKEN'])

for p in gl.projects.list(owned=True):
  print ("=== Project:"+ p.attributes['name'])
  print ("Commits: %d" % len(p.commits.list()))

  if len(p.issues.list()) > 0:
    print ("Issues:")

    for i in p.issues.list(): 
      print("-", wrappers.flat_issue(i))
