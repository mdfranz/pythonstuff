
# From https://www.agiliq.com/blog/2015/07/getting-started-with-celery-and-redis/

import requests

from celery import Celery

app = Celery('hello', broker='redis://172.17.0.1:6379/0')

@app.task

def fetch_url(url):
  resp = requests.get(url)
  print (resp.status_code)

def func(urls):
  for url in urls:
    fetch_url.delay(url)

if __name__ == "__main__":
    func(["http://google.com", "https://amazon.in", "https://facebook.com", "https://twitter.com", "https://alexa.com"])
    
